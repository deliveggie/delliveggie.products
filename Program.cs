﻿using System;
using System.IO;
using System.Threading.Tasks;
using DeliVeggie.Products.Services;
using DeliVeggie.RabbitMq;
using DeliVeggie.Models.Requests;
using DeliVeggie.Models.Responses;
using DeliVeggie.Products.MessageHandlers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DeliVeggie.Products
{
    class Program
    {
        private static ServiceProvider _serviceProvider;
        private static IProductService _service;
        static void Main(string[] args)
        {
            ServicesConfiguration();

            var bus = _serviceProvider.GetService<ISubscriber>();
            _service = _serviceProvider.GetService<IProductService>();

            while (true)
            {
                bus?.Subscribe(HandelMessage);
            }
        }

        private static IResponseMessage HandelMessage(RequestMessage request)
        {
            if (request == null) return new ResponseMessage<object>();
            Console.WriteLine($"Receiving message {request.RequestType}");
            var factory = new HandlerFactory(_service);
            var handler = factory.GetHandler(request);
            var response =  handler.Handle().Result;
            return response;
        }
        
        private static void ServicesConfiguration()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appsettings.json", false)
                .Build();
        
            var configurationSection = configuration.GetSection("ConnectionString");
            _serviceProvider = new ServiceCollection()
                .AddSingleton<IConnectionProvider>(new ConnectionProvider(configurationSection.Value))
                .AddSingleton<IUnitOfWork>(new UnitOfWork(DbContext.Context))
                .AddSingleton<IProductService>( x=> new ProductService(x.GetService<IUnitOfWork>()))
                .AddSingleton<ISubscriber>(x => new Subscriber(x.GetService<IConnectionProvider>()))
                .BuildServiceProvider();
        }
    }
}