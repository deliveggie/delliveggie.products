﻿using System.Threading.Tasks;
using DeliVeggie.Models.Responses;

namespace DeliVeggie.Products.MessageHandlers
{
    public interface IGenericMessageHandler
    {
        Task<IResponseMessage> Handle();
    }
}