﻿using System.Threading.Tasks;
using DeliVeggie.Models.Responses;
using DeliVeggie.Products.Services;

namespace DeliVeggie.Products.MessageHandlers
{
    public class ProductDetailsRequestHandler : IGenericMessageHandler
    {
        private readonly IProductService _productService;
        private readonly string _id;

        public ProductDetailsRequestHandler(IProductService productService,  string id)
        {
            _productService = productService;
            _id = id;
        }

        public async Task<IResponseMessage> Handle()
        {
            var productResponse = await _productService.Get(_id);
            
            var response = new ResponseMessage<ProductDetailsResponse> { Payload = productResponse };

            return response;
        }
    }
}