﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliVeggie.Models.Responses;
using DeliVeggie.Products.Services;

namespace DeliVeggie.Products.MessageHandlers
{
    public class ProductsRequestHandler : IGenericMessageHandler
    {
        private readonly IProductService _productService;

        public ProductsRequestHandler(IProductService productService)
        {
            _productService = productService;
        }

        public async Task<IResponseMessage> Handle()
        {
            var productResponse = await _productService.GetAll();
            
            var response = new ResponseMessage<ICollection<ProductsResponse>> { Payload = productResponse.ToList() };

            return response;
        }
    }
}