﻿using DeliVeggie.Models.Enums;
using DeliVeggie.Models.Requests;
using DeliVeggie.Products.Services;

namespace DeliVeggie.Products.MessageHandlers
{
    public class HandlerFactory
    {
        private readonly IProductService _productService;

        public HandlerFactory(IProductService productService)
        {
            _productService = productService;
        }

        public IGenericMessageHandler GetHandler(RequestMessage message)
        {
            switch (message.RequestType)
            {
                case RequestType.ProductsRequest:
                    return new ProductsRequestHandler(_productService);
                case RequestType.ProductDetailsRequest:
                    return new ProductDetailsRequestHandler(_productService, message.Id);
                default:
                    return null;
            }
        }
    }
}