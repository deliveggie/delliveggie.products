﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliVeggie.Models.Responses;

namespace DeliVeggie.Products.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork Uow;

        public ProductService(IUnitOfWork uow)
        {
            Uow = uow;
        }

        public async Task<IEnumerable<ProductsResponse>> GetAll()
        {
            return (await Uow.ProcessRepository.GetAllAsync())?.Select(x => new ProductsResponse
            {
                Id = x.Id,
                Name = x.Name
            });
        }

        public async Task<ProductDetailsResponse> Get(string id)
        {
            var product = await Uow.ProcessRepository.GetAsync(id);
            
            if(product == null) return new ProductDetailsResponse();

            var today = DateTime.Now.Day;
            var todayPrice = product.PriceWithReduction.FirstOrDefault(x => x.DayOfWeek.GetValueOrDefault() == today);

            if (todayPrice == null)
                return new ProductDetailsResponse
                {
                    Id = product.Id,
                    Name = product.Name,
                    EntryDate = product.EntryDate,
                    PriceWithReduction = product.Price
                };

            return new ProductDetailsResponse
            {
                Id = product.Id,
                Name = product.Name,
                EntryDate = product.EntryDate,
                PriceWithReduction = product.Price.GetValueOrDefault() / todayPrice.Reduction.GetValueOrDefault() * 100
            };
        }
    }
}