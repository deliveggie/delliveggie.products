﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DeliVeggie.Models.Responses;

namespace DeliVeggie.Products.Services
{
    public interface IProductService
    {
        Task<IEnumerable<ProductsResponse>> GetAll();
        Task<ProductDetailsResponse> Get(string id);
    }
}